#!/usr/bin/env bash

java -Djava.awt.headless=true -jar /usr/local/share/plantuml.jar "$@"
