FROM alpine:3.19

ARG PLANTUML_VERSION

RUN apk add --no-cache --update \
        make \
        bash \
        curl \
        openjdk11-jdk \
        graphviz \
        ttf-dejavu \
        fontconfig \
        inotify-tools \
        entr \
        parallel \
    && curl -L -o /usr/local/share/plantuml.jar https://github.com/plantuml/plantuml/releases/download/v${PLANTUML_VERSION}/plantuml-${PLANTUML_VERSION}.jar

COPY scripts/plantuml.sh /usr/local/bin/plantuml
COPY scripts/render.sh /usr/local/bin/render

ENV HOME /tmp

RUN chmod +x /usr/local/bin/plantuml \
    && chmod +x /usr/local/bin/render \
    && parallel --will-cite \
    && echo "will cite" | parallel --citation

WORKDIR "/src"

CMD ["plantuml"]
