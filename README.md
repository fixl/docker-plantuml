# plantuml Image

[![pipeline status](https://gitlab.com/fixl/docker-plantuml/badges/master/pipeline.svg)](https://gitlab.com/fixl/docker-plantuml/-/pipelines)
[![version](https://fixl.gitlab.io/docker-plantuml/version.svg)](https://gitlab.com/fixl/docker-plantuml/-/commits/master)
[![size](https://fixl.gitlab.io/docker-plantuml/size.svg)](https://gitlab.com/fixl/docker-plantuml/-/commits/master)
[![Docker Pulls](https://img.shields.io/docker/pulls/fixl/plantuml)](https://hub.docker.com/r/fixl/plantuml)
[![Docker Stars](https://img.shields.io/docker/stars/fixl/plantuml)](https://hub.docker.com/r/fixl/plantuml)

A Docker container containing [PlantUML](https://plantuml.com/) to generate diagrams from code.

This image can be used with [3 Musketeers](https://3musketeers.io/).

## Build the image

```bash
make build
```

## Inspect the image

```bash
docker inspect --format='{{ range $k, $v := .Config.Labels }}{{ printf "%s=%s\n" $k $v}}{{ end }}' fixl/plantuml:latest
```

## Usage

This image contains a `render` script that allows you to render all `*.puml` files recursively
within a directory structure.

Example:
```
render -o output -b . -w
```

The main driver for this container is to conveniently render PlantUML diagrams for my [Blog] and use
[Hugo's] auto-reload during development.

[Blog]: https://fixl.info
[Hugo's]: https://gohugo.io/

## Changes

See what's changed between releases: https://plantuml.com/changes
